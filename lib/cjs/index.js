"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.make = exports.Loopable = void 0;
const loopable_1 = require("./loopable");
Object.defineProperty(exports, "Loopable", { enumerable: true, get: function () { return loopable_1.Loopable; } });
Object.defineProperty(exports, "make", { enumerable: true, get: function () { return loopable_1.make; } });
