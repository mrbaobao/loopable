"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Loopable = exports.make = void 0;
class Loopable {
    //Construct with a list
    constructor(list) {
        this.list = list;
        //Customizers
        this.customizers = [];
        /**
         * Add customizer
         */
        this.add = (modifier) => {
            this.customizers.push(modifier);
            return this;
        };
        /**
         * Exec
         */
        this.loop = (row) => {
            //Loop
            const count = this.list.length;
            const cCount = this.customizers.length;
            const output = [];
            for (let i = 0; i < count; i++) {
                for (let j = 0; j < cCount; j++) {
                    this.customizers[j](this.list[i], i);
                }
                //Push
                output.push(row(this.list[i], i));
            }
            //Return output
            return output;
        };
    }
}
exports.Loopable = Loopable;
/**
 * Make list loop
 * @param list
 * @returns
 */
const make = (list) => {
    return new Loopable(list);
};
exports.make = make;
