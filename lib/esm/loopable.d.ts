declare type Modifier = (item: any, index: number) => void;
declare class Loopable {
    private readonly list;
    private readonly customizers;
    constructor(list: any[]);
    /**
     * Add customizer
     */
    add: (modifier: Modifier) => Loopable;
    /**
     * Exec
     */
    loop: (row: (item: any, index: number) => any) => any[];
}
/**
 * Make list loop
 * @param list
 * @returns
 */
declare const make: (list: any[]) => Loopable;
export { make, Loopable };
