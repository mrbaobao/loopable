type Modifier = (item: any, index: number) => void
class Loopable {
    //Customizers
    private readonly customizers: Function[] = []
    //Construct with a list
    constructor(private readonly list: any[]) { }

    /**
     * Add customizer
     */
    add = (modifier: Modifier): Loopable => {
        this.customizers.push(modifier)
        return this
    }
    /**
     * Exec
     */
    loop = (row: (item: any, index: number) => any): any[] => {
        //Loop
        const count = this.list.length
        const cCount = this.customizers.length
        const output: any[] = []
        for (let i = 0; i < count; i++) {
            for (let j = 0; j < cCount; j++) {
                this.customizers[j](this.list[i], i)
            }
            //Push
            output.push(row(this.list[i], i))
        }
        //Return output
        return output
    }
}
/**
 * Make list loop
 * @param list 
 * @returns 
 */
const make = (list: any[]): Loopable => {
    return new Loopable(list)
}

//Export
export { make, Loopable }