const { make, Loopable } = require("./lib/cjs/index")
const arrUsers = [
    { name: "Iron Man" },
    { name: "Black Widow" },
    { name: "Captain America" },
    { name: "Peter Parker" },
]
//Log initial
console.log(arrUsers)
//Make it loopable
const loopableList = make(arrUsers)
//Add modifier
loopableList.add((item, index) => {
    item.name = "I am " + item.name
})
//Add modifier
loopableList.add((item, index) => {
    item.age = 28 - index * 2
})
//Loop one time
const output = loopableList.loop((item, index) => {
    item.is_woman = index == 1
    return `${index + 1}.${item.name}, ${item.age}, ${item.is_woman}`
})
console.log(output)
console.log(arrUsers)